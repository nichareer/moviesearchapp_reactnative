import React, {useContext} from 'react';
import {View, FlatList} from 'react-native';
import styles from './Style';
import {Context as MovieContext} from '../../Context/MovieContext';
import Movie from '../../Components/Movie';

const FavouriteMovieListScreen = () => {
  const {state} = useContext(MovieContext);
  return (
    <View style={styles.default}>
      <FlatList
        data={state}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => {
          return <Movie item={item} />;
        }}
      />
    </View>
  );
};

export default FavouriteMovieListScreen;
