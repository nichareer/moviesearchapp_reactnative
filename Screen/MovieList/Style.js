import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: 5,
    marginVertical: 5,
    borderColor: 'lightblue',
    borderWidth: 3,
    borderRadius: 10,
    width: 400,
  },
  detail: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 5,
  },
  image: {
    height: 150,
    width: 100,
    margin: 8,
  },
  title: {
    width: 270,
    fontSize: 18,
    fontWeight: 'bold',
    padding: 3,
  },
  releaseDate: {
    fontSize: 15,
    color: 'grey',
  },
  overview: {
    width: 260,
    padding: 3,
    fontSize: 15,
  },
});

export default styles;
