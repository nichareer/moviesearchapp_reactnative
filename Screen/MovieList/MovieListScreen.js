import React from 'react';
import {View, FlatList} from 'react-native';
import styles from './Style';
import Movie from '../../Components/Movie';

const MovieListScreen = props => {
  const {results} = props.route.params;

  return (
    <View style={styles.default}>
      <FlatList
        data={results}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => {
          return <Movie item={item} />;
        }}
      />
    </View>
  );
};

export default MovieListScreen;
