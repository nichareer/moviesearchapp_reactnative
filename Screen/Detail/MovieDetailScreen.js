import React from 'react';
import {View, Text, Image, Button} from 'react-native';
import styles from './Style';
import FavouriteButton from '../../Components/FavouriteButton';

const DetailScreen = props => {
  const {navigation} = props;
  const {data} = props.route.params;
  navigation.setOptions({
    title: 'Movie Detail',
    headerRight: () => (
      <View style={styles.rightButton}>
        <Button
          title="Back to Search"
          onPress={() => {
            navigation.navigate('SearchScreen');
          }}
        />
      </View>
    ),
  });

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: `https://image.tmdb.org/t/p/w92${data.poster_path}`,
        }}
      />
      <Text style={styles.title}>{data.title}</Text>
      <Text style={styles.average}>Vote Average: {data.vote_average}</Text>
      <Text style={styles.overview}>{data.overview}</Text>
      <FavouriteButton data={data} />
    </View>
  );
};

export default DetailScreen;
