import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'stretch',
    height: 300,
    width: 200,
    marginHorizontal: 105,
  },
  title: {
    fontSize: 18,
    margin: 10,
    fontWeight: 'bold',
  },
  average: {
    fontSize: 18,
    margin: 10,
  },
  overview: {
    fontSize: 18,
    margin: 10,
  },
  rightButton: {
    marginRight: 5,
  },
});

export default styles;
