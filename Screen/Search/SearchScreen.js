import React, {useContext} from 'react';
import {View, Button, FlatList, Text} from 'react-native';
import styles from './Style';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';
import api from '../../APIManager/MovieAPI';
import {Context as SearchContext} from '../../Context/SearchContext';
import MovieSearchBar from '../../Components/SearchBar';

const SearchScreen = props => {
  const {navigation} = props;
  const {state, addSearch} = useContext(SearchContext);

  navigation.setOptions({
    title: 'Search',
    headerRight: () => (
      <View style={styles.rightButton}>
        <Button
          title="Favourite"
          onPress={() => {
            navigation.navigate('FavouriteMovieListScreen');
          }}
        />
      </View>
    ),
  });

  const listPresses = async searchKey => {
    const response = await api.get('/api/movies/search', {
      params: {
        query: searchKey,
        page: 1,
      },
    });
    const data = response.data;
    const results = data.results;
    navigation.navigate('MovieListScreen', {results: results});
    addSearch(searchKey);
  };

  return (
    <View style={styles.container}>
      <MovieSearchBar />
      <FlatList
        data={state}
        keyExtractor={item => item.title}
        renderItem={({item}) => {
          return (
            <TouchableNativeFeedback onPress={() => listPresses(item.title)}>
              <View style={styles.history}>
                <Text style={styles.historyText}>{item.title}</Text>
              </View>
            </TouchableNativeFeedback>
          );
        }}
      />
    </View>
  );
};

export default SearchScreen;
