import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  rightButton: {
    marginRight: 5,
  },
  history: {
    backgroundColor: 'lightblue',
    padding: 5,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 8,
  },
  historyText: {
    fontSize: 18,
    marginVertical: 8,
    marginLeft: 10,
  },
});

export default styles;
