import createContext from './CreateContext';

// const initialState = [
//   {
//     id: 1,
//     title: 'AAAAA',
//     release_date: '2020-06-24',
//     overview: 'overview',
//     vote_average: 0.0,
//     poster_path: 'posterpath',
//   },
//   {
//     id: 2,
//     title: 'BBBBB',
//     release_date: '2020-06-24',
//     overview: 'overview',
//     vote_average: 0.0,
//     poster_path: 'posterpath',
//   },
// ];

const initialState = [];

//Reducer
const movieReducer = (state, action) => {
  switch (action.type) {
    case 'add_movie':
      return [...state, action.payload];
    case 'delete_movie':
      return state.filter(movie => movie.id !== action.payload);
    default:
      return state;
  }
};

//Action
const addMovie = dispatch => {
  return movie => {
    dispatch({
      type: 'add_movie',
      payload: movie,
    });
  };
};

const deleteMovie = dispatch => {
  return id => {
    dispatch({type: 'delete_movie', payload: id});
  };
};

export const {Context, Provider} = createContext(
  movieReducer,
  {addMovie, deleteMovie},
  initialState,
);
