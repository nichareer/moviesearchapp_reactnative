import createContext from './CreateContext';

//const initialState = [{id: 1, title: 'Batman'}, {id: 2, title: 'A'}];
const initialState = [];

//Reducer
const movieReducer = (state, action) => {
  switch (action.type) {
    case 'add_search':
      return [
        {
          title: action.payload,
        },
        ...state.filter(search => search.title !== action.payload),
      ];
    default:
      return state;
  }
};

//Action
const addSearch = dispatch => {
  return title => {
    dispatch({
      type: 'add_search',
      payload: title,
    });
  };
};

export const {Context, Provider} = createContext(
  movieReducer,
  {addSearch},
  initialState,
);
