import React from 'react';
import {View, Text, Image} from 'react-native';
import styles from './Styles';
import {TouchableNativeFeedback} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const Movie = item => {
  const navigation = useNavigation();
  return (
    <TouchableNativeFeedback
      onPress={() =>
        navigation.navigate('MovieDetailScreen', {data: item.item})
      }>
      <View style={styles.container}>
        <Image
          source={{
            uri: `https://image.tmdb.org/t/p/w92${item.item.poster_path}`,
          }}
          style={styles.image}
        />
        <View style={styles.detail}>
          <Text style={styles.title}>{item.item.title}</Text>
          <Text style={styles.releaseDate}>{item.item.release_date}</Text>
          <Text style={styles.overview} numberOfLines={4}>
            {item.item.overview}
          </Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
};

export default Movie;
