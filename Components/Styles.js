import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  searchBar: {
    alignItems: 'stretch',
    backgroundColor: 'red',
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: 5,
    marginVertical: 5,
    borderColor: 'lightblue',
    borderWidth: 3,
    borderRadius: 10,
    width: 400,
  },
  detail: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 5,
  },
  image: {
    height: 150,
    width: 100,
    margin: 8,
  },
  title: {
    width: 270,
    fontSize: 18,
    fontWeight: 'bold',
    padding: 3,
  },
  releaseDate: {
    fontSize: 15,
    color: 'grey',
  },
  overview: {
    width: 260,
    padding: 3,
    fontSize: 15,
  },
  favourite: {
    backgroundColor: 'orange',
    marginTop: 30,
  },
});

export default styles;
