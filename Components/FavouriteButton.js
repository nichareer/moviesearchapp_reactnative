import React, {useContext, useState} from 'react';
import {View, Button} from 'react-native';
import styles from './Styles';
import {Context as MovieContext} from '../Context/MovieContext';

function containsObject(obj, list) {
  for (var i = 0; i < list.length; i++) {
    if (list[i].id === obj.id) {
      return true;
    }
  }
  return false;
}

const FavouriteButton = data => {
  const {state, addMovie, deleteMovie} = useContext(MovieContext);
  var isInFav = containsObject(data.data, state);
  const [buttonText, setButtonText] = useState(
    isInFav ? 'Unfavourtie' : 'Favourite',
  );

  const favouriteButtonPress = () => {
    if (isInFav) {
      deleteMovie(data.data.id);
      setButtonText('Favourite');
    } else {
      addMovie(data.data);
      setButtonText('Unfavourtie');
    }
  };

  return (
    <View style={styles.favourite}>
      <Button
        title={buttonText}
        color="white"
        onPress={() => favouriteButtonPress()}
      />
    </View>
  );
};

export default FavouriteButton;
