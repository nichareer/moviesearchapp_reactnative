import React, {useState, useContext} from 'react';
import styles from './Styles';
import {SearchBar} from 'react-native-elements';
import api from '../APIManager/MovieAPI';
import {Context as SearchContext} from '../Context/SearchContext';
import {useNavigation} from '@react-navigation/native';

const MovieSearchBar = () => {
  const navigation = useNavigation();
  const [search, setSearch] = useState('');
  const {addSearch} = useContext(SearchContext);

  const fetchMovie = async searchKey => {
    const response = await api.get('/api/movies/search', {
      params: {
        query: searchKey,
        page: 1,
      },
    });
    const data = response.data;
    const results = data.results;
    navigation.navigate('MovieListScreen', {results: results});
    addSearch(searchKey);
    setSearch('');
  };

  return (
    <SearchBar
      style={styles.searchBar}
      placeholder="Type Search Key Here"
      onChangeText={newSearch => setSearch(newSearch)}
      value={search}
      platform="ios"
      lightTheme="true"
      onSubmitEditing={() => fetchMovie(search)}
    />
  );
};

export default MovieSearchBar;
