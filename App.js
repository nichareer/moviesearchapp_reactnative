import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SearchScreen from './Screen/Search/SearchScreen';
import MovieListScreen from './Screen/MovieList/MovieListScreen';
import MovieDetailScreen from './Screen/Detail/MovieDetailScreen';
import FavouriteMovieListScreen from './Screen/FavouriteList/FavouriteMovieListScreen';
import {Provider as MovieContextProvider} from './Context/MovieContext';
import {Provider as SearchContextProvider} from './Context/SearchContext';

const Stack = createStackNavigator();

const App = () => {
  return (
    <MovieContextProvider>
      <SearchContextProvider>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="SearchScreen" component={SearchScreen} />
            <Stack.Screen
              name="MovieListScreen"
              component={MovieListScreen}
              options={{title: 'Movie List'}}
            />
            <Stack.Screen
              name="MovieDetailScreen"
              component={MovieDetailScreen}
              options={{title: 'Movie Detail'}}
            />
            <Stack.Screen
              name="FavouriteMovieListScreen"
              component={FavouriteMovieListScreen}
              options={{title: 'Favourite Movie List'}}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </SearchContextProvider>
    </MovieContextProvider>
  );
};

export default App;
